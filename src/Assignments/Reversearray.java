package Assignments;

import java.util.Arrays;

public class Reversearray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		  int[] arr = {1, 2, 3};
	        System.out.println("arr before reverse: " + Arrays.toString(arr) );
	 
	        for(int i=0; i<arr.length/2; i++){
	            int y = arr[i];
	            arr[i] = arr[arr.length -i -1];
	            arr[arr.length -i -1] = y;
	        }
	 
	        System.out.println("arr after reverse: " + Arrays.toString(arr) );
	}

}
