
public class Arithmeticoperators {
	
	
		public static void main(String[] args){
		           //a few numbers
		 int i = 37;
		 int j = 42;
		 double x = 27.475;
		 double y = 7.22;
		 System.out.println("Variable values...");
		 System.out.println("    i = " + i);
		 System.out.println("    j = " + j);
		 System.out.println("    x = " + x);
		 System.out.println("    y = " + y);
		
		 System.out.println("Adding...");
		 System.out.println(" i + j = " + (i + j));
		 System.out.println(" x + y = " + (x + y));
		
		//subtracting numbers
		 System.out.println("subtracting...");
		 System.out.println(" i - j = " + (i - j));
		 System.out.println(" x - y = " + (x - y));
		 
		//Dividing numbers
		 System.out.println("Dividing...");
		 System.out.println(" i / j = " + (i / j));
		 System.out.println(" x / y = " + (x / y));

		//multiplying numbers
		 System.out.println("multiplying...");
		 System.out.println(" i * j = " + (i * j));
		 System.out.println(" x * y = " + (x * y));
		 
		//Remainder  numbers
		 System.out.println("Remainder...");
		 System.out.println(" i % j = " + (i % j));
		 System.out.println(" x % y = " + (x % y));
		
		 //Mixing  numbers
		 System.out.println("Mixing...");
		 System.out.println(" i + j = " + (i + j));
		 System.out.println(" x - y = " + (x - y));
		 System.out.println(" x * j = " + (x * j));
}
}