
public class Relationoperators {
	
	public static void main (String[] args) {
		
		
		int i = 10;
		int j = 20;
		int y = 30;
		System.out.println("Variable values...");
		System.out.println("    i = " +i);
		System.out.println("    j = " +j);
	    System.out.println("    y = " +y);
	    
	    //greaterthan
	    
	    System.out.println("Greater than...");
		System.out.println("    i>j = " +(i>j));
		System.out.println("    j>y = " +(j>y));
	    System.out.println("    y>i = " +(y>i));
	    
	  //greater than or equal to
	    System.out.println("Greater than or equal to...");
		System.out.println("    i>=j = " +(i>=j));
		System.out.println("    j>=j = " +(j>=j));
	    System.out.println("    y>=i = " +(y>=i));
	    
     //Lessthan
	    
	    System.out.println("Less than...");
		System.out.println("    i<j = " +(i<j));
		System.out.println("    j<y = " +(j<y));
	    System.out.println("    y<i = " +(y<i));
	    
	  //Less than or equal to
	    System.out.println("Less than or equal to...");
		System.out.println("    i<=j = " +(i<=j));
		System.out.println("    j<=j = " +(j<=j));
	    System.out.println("    y<=i = " +(y<=i));
	    
      //Equal to
	    
	    System.out.println("Equal to...");
		System.out.println("    i==j = " +(i==j));
		System.out.println("    j==y = " +(j==y));
	    System.out.println("    y==i = " +(y==i));
	    
     //not Equal to
	    
	    System.out.println("Not Equal than...");
		System.out.println("    i!=j = " +(i!=j));
		System.out.println("    j!=y = " +(j!=y));
	    System.out.println("    y!=i = " +(y!=i));
	    
	}
	

}
